package cz.emphasis.textdetection.helpers;

import cz.emphasis.textdetection.helpers.BresenhamLine;
import cz.emphasis.textdetection.helpers.DiscretePoint;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BresenhamLineTests {

    @Test
    public void can_iterate(){
        can_iterate_test(false, false, 11, p0, q0);
        can_iterate_test(true, false, 10, p1, q0);
        can_iterate_test(false, true, 10, p0, q1);
        can_iterate_test(true, true, 9, p1, q1);
    }

    private void can_iterate_test(boolean skipFirst, boolean skipLast, int size, DiscretePoint first, DiscretePoint last){
        DiscretePoint p = new DiscretePoint(30, 30);
        DiscretePoint q = new DiscretePoint(40, 40);
        BresenhamLine line = new BresenhamLine(p, q, skipFirst, skipLast);

        List<DiscretePoint> points = new ArrayList<>();
        for (DiscretePoint r : line){
            points.add(r);
        }

        assertEquals(points.size(), size);
        assertEquals(points.get(0), first);
        assertEquals(points.get(points.size() - 1), last);
    }

    private static final DiscretePoint p0 = new DiscretePoint(30, 30);
    private static final DiscretePoint p1 = new DiscretePoint(31, 31);
    private static final DiscretePoint q1 = new DiscretePoint(39, 39);
    private static final DiscretePoint q0 = new DiscretePoint(40, 40);

    @Test
    public void can_find_intersection(){
        int w = 278;
        int h = 240;

        BresenhamLine topDown = new BresenhamLine(new DiscretePoint(140,33), 4.702806985766306d, w, h);
        BresenhamLine topUp = new BresenhamLine(new DiscretePoint(140,58), 1.5578679048089175d, w, h);
        BresenhamLine bottomUp = new BresenhamLine(new DiscretePoint(140,217), 1.5795735183803874d, w, h);
        BresenhamLine bottomDown = new BresenhamLine(new DiscretePoint(140,193), 4.7257875053446705d, w, h);
        BresenhamLine leftRight = new BresenhamLine(new DiscretePoint(59,125), 6.2811293214442685d, w, h);
        BresenhamLine leftLeft = new BresenhamLine(new DiscretePoint(90,125), 3.138948550809314d, w, h);
        BresenhamLine rightLeft = new BresenhamLine(new DiscretePoint(220,125), 3.1433540043474006d, w, h);
        BresenhamLine rightRight = new BresenhamLine(new DiscretePoint(189,125), 0.003991532422873867d, w, h);

        assertTrue(abs(topDown.getEnd().x - topDown.getStart().x) < 5 && topDown.getEnd().y == h);
        assertTrue(abs(topUp.getEnd().x - topUp.getStart().x) < 5 && topUp.getEnd().y == 0);
        assertTrue(abs(bottomUp.getEnd().x - bottomUp.getStart().x) < 5 && bottomUp.getEnd().y == 0);
        assertTrue(abs(bottomDown.getEnd().x - bottomDown.getStart().x) < 5 && bottomDown.getEnd().y == h);
        assertTrue(abs(leftRight.getEnd().y - leftRight.getStart().y) < 5 && leftRight.getEnd().x == w);
        assertTrue(abs(leftLeft.getEnd().y - leftLeft.getStart().y) < 5 && leftLeft.getEnd().x == 0);
        assertTrue(abs(rightLeft.getEnd().y - rightLeft.getStart().y) < 5 && rightLeft.getEnd().x == 0);
        assertTrue(abs(rightRight.getEnd().y - rightRight.getStart().y) < 5 && rightRight.getEnd().x == w);
    }
}