package cz.emphasis.textdetection;

import cz.emphasis.textdetection.helpers.DiscretePoint;
import cz.emphasis.textdetection.helpers.MatByteIndexer;
import cz.emphasis.textdetection.helpers.StreamHelpers;
import org.apache.commons.io.IOUtils;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Test {
    // Compulsory
    static{
        //System.setProperty("java.library.path", "c:/opencv/build/java");
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        nu.pattern.OpenCV.loadShared(); //add this
    }

    private static final String IMAGE_PATH = "test01.png";

    public static void main(String[] args) throws Exception {
        Test test = new Test();
        test.textDetection();
    }

    public void textDetection() throws Exception {
        Mat source = getImage(IMAGE_PATH);
        if (source == null)
            throw new Exception("Unable to load the image: " + IMAGE_PATH);

        double scale = 2;
        long t1 = System.nanoTime();

        TextDetection detection = new TextDetection();
        int i = 0;
        for (; i < 1; i++)
            detection.findText(source, scale); // Takes cca 5s on Intel Core i7-3770T (8190 passmark) for 1920x1080 resolution

        long t2 = System.nanoTime();
        long td = TimeUnit.NANOSECONDS.toMillis(t2-t1);
        System.err.println("ELAPSED " + td + " = " + i + " x " + td/i);

        System.err.println("RAYS: " + detection.rays.size());

        //try (PrintWriter writer = new PrintWriter("swt4.txt")){
        //    writer.println(detection.swt.dump());
        //}

        Size size = detection.size;
        int w = detection.width;
        int h = detection.height;

        Mat labels = new Mat(new Size(w, h), CvType.CV_16U, Scalar.all(Short.MAX_VALUE/2));
        for (int y=0; y < h; y++){
            for (int x=0; x < w; x++){
                labels.put(y, x, detection.labels.get(x,y));
            }
        }

        Mat regions = new Mat(new Size(w,h), CvType.CV_8U, Scalar.all(255));
        for (Region r : detection.regions){
            for (DiscretePoint p : r){
                p.put(regions, 20);
            }
        }

        Mat lined = new Mat(new Size(w, h), CvType.CV_8U, Scalar.all(0));
        for (TextLine line : detection.lines){
            StreamHelpers.slidingWindow(line.line, 2)
                .forEach(sub -> {
                    Point p1 = new Point(sub.get(0).centroidX, sub.get(0).centroidY);
                    Point p2 = new Point(sub.get(1).centroidX, sub.get(1).centroidY);
                    Imgproc.line(lined, p1, p2, Scalar.all(50), 1);
                });

            Rect box = line.boundingBox();
            Point p1 = new Point(box.x, box.y);
            Point p2 = new Point(box.x + box.width, box.y + box.height);
            Imgproc.rectangle(lined, p1, p2, Scalar.all(120), 1);
        }

        for (Region r : detection.regions){
            DiscretePoint p1 = new DiscretePoint((int)r.centroidX, (int)r.centroidY);
            p1.put(lined, 255);
        }

        MatByteIndexer lnd = new MatByteIndexer(lined);
        MatByteIndexer edges = new MatByteIndexer(detection.edges);
        for (int y = 0; y < h; y++){
            for (int x = 0; x <w; x++){
                int edge = edges.get(x,y);
                int prev = lnd.get(x,y);
                if (edge > 0 && prev == 0)
                    lnd.put(x,y,(short)60);
            }
        }
        lnd.close();

        Map<Region, Set<TextLine>> cand = new TextLineClustering().FindIntersections(detection.regions);
        Mat candidates = new Mat(new Size(w,h), CvType.CV_8U, Scalar.all(0));
        for (Map.Entry<Region, Set<TextLine>> e : cand.entrySet()){
            Region r = e.getKey();
            if (r.label != 19)
                continue;
            Set<TextLine> intersection = e.getValue();
            for(TextLine line : intersection) {
                Region src = line.getFirst();
                Region dst = line.getLast();
                Imgproc.line(candidates, src.centroid, dst.centroid, Scalar.all(120));
                DiscretePoint.round(src.centroid).put(candidates, 255);
                DiscretePoint.round(dst.centroid).put(candidates, 255);
            }
            DiscretePoint.round(r.centroid).put(candidates, 255);
        }
        MatByteIndexer cnd = new MatByteIndexer(candidates);
        for (int y = 0; y < h; y++){
            for (int x = 0; x <w; x++){
                int edge = edges.get(x,y);
                int prev = cnd.get(x,y);
                if (edge > 0)
                    cnd.put(x,y,(short)60);
            }
        }
        cnd.close();
        int dy = 10;
        for (Region r : detection.regions){
            DiscretePoint.round(r.centroid).put(candidates,255);
            //Imgproc.putText(candidates, ""+r.label, new Point(r.centroidX-5, r.centroidY - dy), Core.FONT_HERSHEY_PLAIN, 1.0, Scalar.all(180));
            dy *= -1;
        }

        Core.normalize(detection.gray, detection.gray, 0x00, 0xFF, Core.NORM_MINMAX, CvType.CV_8U);
        Core.normalize(detection.edges, detection.edges, 0x00, 0xFF, Core.NORM_MINMAX, CvType.CV_8U);
        Core.normalize(labels, labels, 0x00, 0xFF, Core.NORM_MINMAX, CvType.CV_8U);

        //double resultScale = 2;
        //for (Mat m : Arrays.asList(detection.gray, detection.edges, labels, regions, candidates, lined))
        //    Imgproc.resize(m, m, new Size(w * resultScale, h * resultScale));

        //Imgproc.resize(detection.gray, detection.gray, new Size(w/2,h/2));
        //Imgproc.resize(detection.edges, detection.edges, new Size(w/2,h/2));
        //Imgproc.resize(labels, labels, new Size(w/2,h/2));
        //Imgproc.resize(regions, regions, new Size(w/2,h/2));
        //Imgproc.resize(lined, lined, new Size(w/2,h/2));

        try {
            BufferedImage bi = mat2BufferedImage(lined);
            File outputfile = new File("result.png");
            ImageIO.write(bi, "png", outputfile);
        } catch (IOException e) {

        }

        new ImageBuilder()
            .addImage(detection.gray, "gray")
            .addImage(detection.edges, "edges")
            .addImage(labels, "labels")
            .addImage(regions, "regions")
            .addImage(candidates, "candidates")
            .addImage(lined, "lined")
            .show();
    }

    private class ImageBuilder{
        List<NamedBufferedImage> images = new ArrayList<>();

        public void addImage(Mat mat){
            addImage(mat, "");
        }

        public ImageBuilder addImage(Mat mat, String name){
            BufferedImage image = mat2BufferedImage(mat);
            images.add(new NamedBufferedImage(image, name));
            return this;
        }

        public void show(){
            int width = images.stream().mapToInt(x -> x.image.getWidth()).sum();
            int height = images.get(0).image.getHeight();

            DefaultListModel<NamedBufferedImage> model = new DefaultListModel<>();
            images.forEach(x -> model.addElement(x));

            ImageCellRenderer renderer = new ImageCellRenderer();

            JList list = new JList(model);
            list.setCellRenderer(renderer);
            list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
            list.setVisibleRowCount(2);

            JScrollPane scroll = new JScrollPane(list);
            //scroll.setPreferredSize(new Dimension(width,height));

            JFrame frame = new JFrame();
            frame.add(scroll);
            frame.pack();
            frame.setLocationRelativeTo(null); // Centered
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }
    }

    public class NamedBufferedImage{
        public final String name;
        public final BufferedImage image;

        public NamedBufferedImage(BufferedImage image, String name){
            this.name = name;
            this.image = image;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public class ImageCellRenderer extends DefaultListCellRenderer {

        public ImageCellRenderer(){

        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (c instanceof JLabel && value instanceof NamedBufferedImage){
                JLabel label = (JLabel)c;
                NamedBufferedImage img = (NamedBufferedImage)value;
                ImageIcon icon = new ImageIcon(img.image);
                label.setIcon(icon);
                label.setVerticalTextPosition(TOP);
                label.setHorizontalTextPosition(CENTER);
            }

            return c;
        }
    }

    // http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
    public BufferedImage mat2BufferedImage(Mat mat) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( mat.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = mat.channels() * mat.cols() * mat.rows();
        byte [] b = new byte[bufferSize];
        mat.get(0,0, b); // get all the pixels
        BufferedImage image = new BufferedImage(mat.cols(),mat.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    private Mat getImage(String fileName){
        byte[] result = null;

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream stream = classLoader.getResourceAsStream(fileName);
        try {
            result = IOUtils.toByteArray(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Mat mat = Imgcodecs.imdecode(new MatOfByte(result), Imgcodecs.IMREAD_UNCHANGED);
        return mat;
    }
}