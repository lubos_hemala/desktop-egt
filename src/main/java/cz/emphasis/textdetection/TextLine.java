package cz.emphasis.textdetection;

import cz.emphasis.textdetection.helpers.DiscretePoint;
import cz.emphasis.textdetection.helpers.Helpers;
import cz.emphasis.textdetection.helpers.StreamHelpers;
import org.opencv.core.Rect;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.PI;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class TextLine{
    protected final List<Region> line = new ArrayList<>();
    protected final Set<Region> regions = new HashSet<>();

    public TextLine(Region r1, Region r2){
        line.add(r1);
        line.add(r2);
        regions.addAll(line);
    }

    public TextLine(List<Region> regions){
        line.addAll(regions);
        this.regions.addAll(regions);
    }

    public int size(){ return regions.size(); }

    public Region getFirst(){ return line.get(0); }
    public Region getSecond(){ return line.get(1); }
    public Region getLast(){ return line.get(line.size()-1); }
    public Region getSecondLast(){ return line.get(line.size()-2); }

    private Double centroidDirection;
    public double centroidDirection(){
        if (centroidDirection == null){
            centroidDirection = getFirst().centroidDirection(getLast());
        }
        return centroidDirection;
    }

    private Double bottomCenterDirection;
    public double bottomCenterDirection(){
        if (bottomCenterDirection == null){
            bottomCenterDirection = getFirst().bottomCenterDirection(getLast());
        }
        return bottomCenterDirection;
    }

    public boolean isCentroidOpposite(double beta, double precision){
        return
            Helpers.isOpposite(centroidDirection(), beta, precision) ||
            Helpers.isOpposite(centroidDirection(), beta + PI, precision);
    }

    public boolean isBottomCenterOpposite(double beta, double precision){
        return
            Helpers.isOpposite(bottomCenterDirection(), beta, precision) ||
            Helpers.isOpposite(bottomCenterDirection(), beta + PI, precision);
    }

    public double distanceAt(int index){
        int i1 = max(0, index - 1);
        int i2 = min(index + 2, line.size());
        List<Region> sub = line.subList(i1, i2);
        double distance = StreamHelpers.slidingWindow(sub, 2)
            .mapToDouble(s -> s.get(0).centroidDistance(s.get(1)))
            .sum();

        distance /= (sub.size() -1);
        return distance;
    }

    private Rect boundingBox;
    public Rect boundingBox(){
        if (boundingBox == null) {
            int minX = Integer.MAX_VALUE, maxX = 0, minY = Integer.MAX_VALUE, maxY = 0;
            for (Region region : line) {
                if (region.minX < minX)
                    minX = region.minX;
                if (region.maxX > maxX)
                    maxX = region.maxX;
                if (region.minY < minY)
                    minY = region.minY;
                if (region.maxY > maxY)
                    maxY = region.maxY;
            }

            int width = maxX - minX;
            int height = maxY - minY;
            boundingBox = new Rect(minX, minY, width, height);
        }
        return boundingBox;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextLine textLine = (TextLine) o;
        return regions.equals(textLine.regions);
    }

    @Override
    public int hashCode() {
        return regions.hashCode();
    }

    @Override
    public String toString() {
        Region r1 = getFirst(), r2 = getLast();
        boolean direction = r1.minX < r2.minX;
        List<Region> line1 = new ArrayList<>(line);
        if (!direction)
            Collections.reverse(line1);
        List<String> c1 = line1.stream().map(x -> x.label + ":" + DiscretePoint.round(x.centroid)).collect(Collectors.toList());
        return String.join(" → ", c1);
    }
}