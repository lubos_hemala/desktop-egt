package cz.emphasis.textdetection;

import cz.emphasis.textdetection.helpers.DiscretePoint;
import cz.emphasis.textdetection.helpers.UnorderedPair;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static cz.emphasis.textdetection.TextDetection.TEXTLINE_OPPOSITE_PRECISION_HIGH;
import static cz.emphasis.textdetection.TextDetection.TEXTLINE_OPPOSITE_PRECISION_LOW;
import static java.lang.Math.min;

public class TextCluster extends UnorderedPair<DirectedTextLine> implements Comparable<TextCluster> {
    private final DirectedTextLine l1;
    private final DirectedTextLine l2;
    public final Region root;

    public TextCluster(DirectedTextLine l1, DirectedTextLine l2, Region root){
        super(l1, l2);
        this.l1 = l1;
        this.l2 = l2;
        this.root = root;
    }

    public TextCluster(TextLine l1, TextLine l2, Region root){
        this(new DirectedTextLine(l1, root), new DirectedTextLine(l2, root), root);
    }

    public static TextCluster create(TextLine l1, TextLine l2, Region root){
        return new TextCluster(l1, l2, root);
    }

    public List<TextLine> lines(){
        return Arrays.asList(l1.line, l2.line);
    }
    public List<DirectedTextLine> directedLines(){
        return Arrays.asList(l1, l2);
    }

    private Double mergeDistance;
    public double mergeDistance(){
        if (mergeDistance == null) {
            double d1 = l1.centroidDistance1st2nd();
            double d2 = l2.centroidDistance1st2nd();
            mergeDistance = d1 + d2;
        }
        return mergeDistance;
    }

    private Boolean canConnect;
    public Boolean canConnect(){
        if (canConnect != null)
            return canConnect;

        if (l1.equals(l2))
            return (canConnect = false);

        Region r1 = l1.getSecond();
        Region r2 = l2.getSecond();

        double precision = TEXTLINE_OPPOSITE_PRECISION_LOW;

        double w3 = (r1.width + root.width + r2.width)/3;
        double hg1 = l1.horizontalGap1st2nd();
        double hg2 = l2.horizontalGap1st2nd();
        if (hg1 < w3*2 && hg2 < w3*2)
            precision = TEXTLINE_OPPOSITE_PRECISION_HIGH;
        if (hg1 + hg2 > w3*5)
            return (canConnect = false);

        double c3 = l1.centroidDistance1st2nd() + l2.centroidDistance1st2nd();
        double wh3 = (min(r1.width, r1.height) + min(root.width, root.height) + min(r2.width, r2.height))/3;
        if (c3 > wh3*10)
            return (canConnect = false);

        boolean isOpposite = l1.isOpposite(l2, precision);
        return (canConnect = isOpposite);
    }

    public TextLine connect() {
        return l1.connect(l2);
    }

    public int size(){
        return l1.line.size() + l2.line.size();
    }

    @Override
    public int compareTo(TextCluster o) {
        // Shorter centroidDistance first
        int diff = Double.compare(mergeDistance(), o.mergeDistance());
        if (diff != 0)
            return diff;

        // Longer size first
        diff = -(this.size() - o.size());
        if (diff != 0)
            return diff;

        diff = this.hashCode() - o.hashCode();
        return diff;
    }

    @Override
    public int hashCode() {
        return l1.hashCode() + l2.hashCode();
    }

    @Override
    public String toString() {
        boolean direction = l1.getLast().minX < l2.getLast().minX;
        DirectedTextLine d1 = direction ? l1 : l2, d2 = direction ? l2 : l1;
        List<Region> line1 = d1.rotate ? d1.getSourceLine() : d1.getReverseLine();
        List<Region> line2 = d2.rotate ? d2.getReverseLine() : d2.getSourceLine();
        List<String> c1 = line1.stream().map(x -> x.label + ":" + DiscretePoint.round(x.centroid)).collect(Collectors.toList());
        List<String> c2 = line2.stream().map(x -> x.label + ":" + DiscretePoint.round(x.centroid)).collect(Collectors.toList());
        return String.join(" < ", c1) + " @" + root.label + " " + String.join(" > ", c2);
    }
}
