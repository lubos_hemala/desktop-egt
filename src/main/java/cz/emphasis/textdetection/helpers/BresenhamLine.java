package cz.emphasis.textdetection.helpers;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

import static java.lang.Math.*;

public class BresenhamLine implements Iterator<DiscretePoint>, Iterable<DiscretePoint> {

    private int px, py, rx, ry, qx, qy, dx, dy, ix, iy, err;
    private boolean skipFirst;
    private boolean skipLast;
    private boolean isCompleted = false;
    private double theta = -1;
    private DiscretePoint start;
    private DiscretePoint end;

    public DiscretePoint getStart(){
        return start.clone();
    }
    public DiscretePoint getEnd(){
        return end.clone();
    }

    public double length(){
        return start.distance(end);
    }

    public BresenhamLine(DiscretePoint start, double theta, int width, int height, boolean skipFirst, boolean skipLast){
        this(start.x, start.y, theta, width, height, skipFirst, skipLast);
    }
    public BresenhamLine(DiscretePoint start, double theta, int width, int height){
        this(start.x, start.y, theta, width, height, false, false);
    }

    public BresenhamLine(int px, int py, double theta, int width, int height, boolean skipFirst, boolean skipLast){
        this.skipFirst = skipFirst;
        this.skipLast = skipLast;

        double dx = cos(theta);
        double dy = -sin(theta); // Vertical coordinates are inverted for image indexing (from 0 to +Infinity)

        if (dx == 0) {
            qx = px;
            qy = dy > 0 ? height : 0;
        } else if (dy == 0){
            qy = py;
            qx = dx > 0 ? width : 0;
        } else {
            // Determine the centroidDistance to the boundary
            double hx = dx > 0 ? width - px : px;
            double hy = dy > 0 ? height - py : py;

            // Check to see which boundary line is reached first
            double nx = hx / abs(dx);
            double ny = hy / abs(dy);

            if (nx < ny){
                qx = dx > 0 ? width : 0;
                qy = (int)floor(py + dy*nx);
            } else if (nx > ny){
                qy = dy > 0 ? height : 0;
                qx = (int)floor(px + dx*ny);
            } else {
                qx = dx > 0 ? width : 0;
                qy = dy > 0 ? height : 0;
            }
        }

        initialize(px, py, qx, qy);
    }

    public BresenhamLine(DiscretePoint p, DiscretePoint q){
        this(p.x, p.y, q.x, q.y, false, false);
    }
    public BresenhamLine(DiscretePoint p, DiscretePoint q, boolean skipFirst, boolean skipLast){
        this(p.x, p.y, q.x, q.y, skipFirst, skipLast);
    }
    public BresenhamLine(int px, int py, int qx, int qy, boolean skipFirst, boolean skipLast){
        this.skipFirst = skipFirst;
        this.skipLast = skipLast;

        initialize(px, py, qx, qy);
    }

    private void initialize(int px, int py, int qx, int qy){
        this.px = px;
        this.py = py;
        this.qx = qx;
        this.qy = qy;

        this.start = new DiscretePoint(px, py);
        this.end = new DiscretePoint(qx, qy);

        reset();
    }

    public void reset(){
        isCompleted = false;

        dx = Math.abs(qx - px);
        dy = Math.abs(qy - py);

        ix = px < qx ? 1 : -1;
        iy = py < qy ? 1 : -1;

        err = dx - dy;

        rx = px;
        ry = py;

        next = null;
    }

    @Override
    public boolean hasNext() {
        if (isCompleted)
            return false;

        if (next == null){
            next = new DiscretePoint(px, py);
            if (skipFirst && rx == px && ry == py)
                advance();
        }

        if (skipLast && next.x == qx && next.y == qy)
            isCompleted = true;

        return !isCompleted;
    }

    private DiscretePoint next;

    @Override
    public DiscretePoint next() {
        if (!hasNext())
            return null;

        if (next.x == qx && next.y == qy)
            isCompleted = true;

        DiscretePoint result = next.clone();
        advance();
        return result;
    }

    private void advance(){
        int p = 2 * err;
        if (p > -dy) {
            err -= dy;
            rx += ix;
        }
        if (p < dx) {
            err += dx;
            ry += iy;
        }

        if (next == null)
            next = new DiscretePoint(rx, ry);

        next.x = rx;
        next.y = ry;
    }

    public Stream<DiscretePoint> points(){
        return StreamHelpers.asStream(iterator());
    }

    @Override
    public Iterator<DiscretePoint> iterator() {
        return new BresenhamLine(start, end, skipFirst, skipLast);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BresenhamLine that = (BresenhamLine) o;
        return
            px == that.px &&
            py == that.py &&
            qx == that.qx &&
            qy == that.qy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(px, py, qx, qy);
    }

    @Override
    public String toString() {
        return start + " → " + end + (theta != -1 ? " theta:" + theta : "");
    }
}