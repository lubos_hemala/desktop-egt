package cz.emphasis.textdetection.helpers;

import java.util.HashSet;
import java.util.Set;

public class UnorderedPair<T> {

    private final T first;
    private final T second;
    private final Set<T> values = new HashSet<>();

    public UnorderedPair(T first, T second) {
        this.first = first;
        this.second = second;
        this.values.add(first);
        this.values.add(second);
    }

    /**
     * Hash code is symmetrical.
     * @return
     */
    @Override
    public int hashCode() {
        T key = first;
        T value = second;
        return (key != null ? key.hashCode() : 0) + (value != null ? value.hashCode() : 0);
    }

    /**
     * Equals is symmetrical.
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof UnorderedPair) {
            UnorderedPair pair = (UnorderedPair) o;
            return values.equals(pair.values);
        }
        return false;
    }

    public static <T> UnorderedPair<T> create(T first, T second) {
        return new UnorderedPair<>(first, second);
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
