package cz.emphasis.textdetection.helpers;

public abstract class Array2D {
    private final int width;

    protected Array2D(int width, int height){
        this.width = width;
    }

    public final int at(int x, int y){
        int distY = y*width;
        return distY + x;
    }
}
