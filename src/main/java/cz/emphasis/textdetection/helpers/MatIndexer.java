package cz.emphasis.textdetection.helpers;

import org.opencv.core.Mat;

public abstract class MatIndexer implements AutoCloseable {

    protected final Mat source;
    protected final int width;
    protected final int size;

    protected MatIndexer(Mat source){
        this.source = source;
        this.width = source.width();
        this.size = (int) (source.total() * source.channels());
    }

    protected final int at(int x, int y){
        return x + y*width;
    }
}
