package cz.emphasis.textdetection.helpers;

import org.opencv.core.Mat;

public class MatFloatIndexer extends MatIndexer {
    private final float[] buffer;

    public MatFloatIndexer(Mat source){
        super(source);
        this.buffer = new float[size];
        source.get(0, 0, buffer);
    }

    public float get(int x, int y){
        return buffer[at(x,y)];
    }

    public void put (int x, int y, float value){
        buffer[at(x,y)] = value;
    }

    public float get(DiscretePoint p){
        return get(p.x, p.y);
    }

    public void put(DiscretePoint p, float value){
        put(p.x, p.y, value);
    }

    @Override
    public void close() throws Exception {
        source.put(0, 0, buffer);
    }
}
