package cz.emphasis.textdetection.helpers;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.Comparator;
import java.util.Objects;

public class DiscretePoint {
    public int x,y;

    public DiscretePoint(){}
    public DiscretePoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public static DiscretePoint round(Point p){
        return new DiscretePoint((int)Math.round(p.x), (int)Math.round(p.y));
    }

    public double distance(DiscretePoint other){
        return Helpers.distance(this, other);
    }

    public boolean isInside(int width, int height){
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public void moveTo(int x, int y){
        this.x = x;
        this.y = y;
    }

    public DiscretePoint shiftBy(int x, int y){
        return new DiscretePoint(this.x + x, this.y + y);
    }

    public double get(Mat mat){
        try {
            return mat.get(y, x)[0];
        } catch (NullPointerException ex){
            System.err.println(this + ": unable to get value from " + mat);
        }
        return 0;
    }

    public void put(Mat mat, double value){
        mat.put(y,x, value);
    }

    public static final ComparatorDyDx comparatorDyDx = new ComparatorDyDx();

    public static class ComparatorDyDx implements Comparator<DiscretePoint> {

        @Override
        public int compare(DiscretePoint o1, DiscretePoint o2) {
            int dy =  o1.y - o2.y;
            if (dy != 0)
                return dy;

            int dx = o1.x - o2.x;
            return dx;
        }
    }

    public DiscretePoint clone(){
        return new DiscretePoint(x,y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiscretePoint that = (DiscretePoint) o;

        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "{" + x + ", " + y + "}";
    }
}